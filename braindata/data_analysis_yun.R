require(dplyr)
require(plyr)

## demo
data_demo=read.csv('idp_dti_subject_complete_300.csv',header=T)
data_demo$SEX<-revalue(data_demo$SEX, c("M"="1", "F"="2"))
data_demo$dx.1ad.2mci.3smi<-factor(data_demo$dx.1ad.2mci.3smi)
data_demo$dx2.1adonly.2adwithsmallvv.3mci.4.smi<-factor(data_demo$dx2.1adonly.2adwithsmallvv.3mci.4.smi)

data_demo$dx_ad_smi<-revalue(data_demo$dx.1ad.2mci.3smi, c("1"="1", "3"="0","2"="NaN"))
data_demo$dx_mci_smi<-revalue(data_demo$dx.1ad.2mci.3smi, c("2"="1", "3"="0","1"="NaN"))
data_demo$dx_ad_mci<-revalue(data_demo$dx.1ad.2mci.3smi, c("1"="1", "2"="0","3"="NaN"))
data_demo$dx_adonly_smi<-revalue(data_demo$dx2.1adonly.2adwithsmallvv.3mci.4.smi, c("1"="1", "4"="0","3"="NaN","2"="NaN"))
data_demo$dx_adonly_mci<-revalue(data_demo$dx2.1adonly.2adwithsmallvv.3mci.4.smi, c("1"="1", "3"="0","4"="NaN","2"="NaN"))
data_demo$dx_adonly_adwithsmallvv<-revalue(data_demo$dx2.1adonly.2adwithsmallvv.3mci.4.smi, c("1"="1", "2"="0","3"="NaN","4"="NaN"))
write.csv(data_demo,file='idp_dti_subject_complete_300_updated.csv',row.names=F)

## hippo
data_hippo_vol=read.csv('hippo_vol.csv')
data_hippo_md=read.csv('hippo_md.csv')
data_hippo_fa=read.csv('hippo_fa_v2.csv')


tmp=merge(data_hippo_md,data_hippo_fa,by="subject")
data_hippo_all=merge(tmp,data_hippo_vol,by="subject") ######################################
data_0_hippo=merge(data_demo, data_hippo_all,by="subject")

## cortical
data_cor=read.csv('fs_cortical_area_thick_vol_curv_v2.csv')
data_cor_2009=read.csv('fs_cortical_2009_area_thick_vol_curv.csv')
data_cor_all=merge(data_cor,data_cor_2009,by="subject")######################################
data_0_cortical=merge(data_demo, data_cor_all,by="subject")

## subcortical
data_subcor=read.csv('fs_subcortical.csv')######################################



## concatenate
t=merge(data_demo,data_hippo_all,by="subject")
t=merge(t,data_cor_all,by="subject")
t=merge(t,data_subcor,by="subject")
data_1_mor=t
write.csv(data_1_mor,file='data_1_mor.csv',row.names = F,na='NaN')
# connectome: order of concatenation: aparc_count -> aparc_lenth -> aparc2009_count -> aparc2009_length


#########Correlation and Ranking data by correlation
t<-data_1_mor[, 12:959]
y=as.numeric(data_1_mor[, 2])

correlations <- vapply(
  t,
  function(x)
  {
    cor(y, x)
  },
  numeric(1)
)

correlations[which.max(abs(correlations))]
data_1_mor_rank=rank(correlations)

correlations[rank(correlations)<101]
data_1_mor_select_100=data.frame(data_1_mor[,1:11],t[,rank(correlations)<101])
write.csv(data_1_mor_select_100,file='data_1_mor_select_100.csv',row.names=F,na="NaN")

data_1_mor_select_200=data.frame(data_1_mor[,1:11],t[,rank(correlations)<201])
write.csv(data_1_mor_select_200,file='data_1_mor_select_200.csv',row.names=F,na="NaN")

data_1_mor_select_300=data.frame(data_1_mor[,1:11],t[,rank(correlations)<301])
write.csv(data_1_mor_select_300,file='data_1_mor_select_300.csv',row.names=F,na="NaN")

data_1_mor_select_400=data.frame(data_1_mor[,1:11],t[,rank(correlations)<401])
write.csv(data_1_mor_select_400,file='data_1_mor_select_400.csv',row.names=F,na="NaN")

data_1_mor_select_500=data.frame(data_1_mor[,1:11],t[,rank(correlations)<501])
write.csv(data_1_mor_select_500,file='data_1_mor_select_500.csv',row.names=F,na="NaN")

data_1_mor_select_600=data.frame(data_1_mor[,1:11],t[,rank(correlations)<601])
write.csv(data_1_mor_select_600,file='data_1_mor_select_600.csv',row.names=F,na="NaN")

data_1_mor_select_700=data.frame(data_1_mor[,1:11],t[,rank(correlations)<701])
write.csv(data_1_mor_select_700,file='data_1_mor_select_700.csv',row.names=F,na="NaN")

data_1_mor_select_800=data.frame(data_1_mor[,1:11],t[,rank(correlations)<801])
write.csv(data_1_mor_select_800,file='data_1_mor_select_800.csv',row.names=F,na="NaN")

data_1_mor_select_900=data.frame(data_1_mor[,1:11],t[,rank(correlations)<901])
write.csv(data_1_mor_select_900,file='data_1_mor_select_900.csv',row.names=F,na="NaN")


#for (i in 1:7056){
#names(data_cntm_aparc_count)[1]<-'aparc_count_1'
#}





#####connectome data#############################################################################
data_cntm_aparc_count=read.csv('connectome_aparc_count_reshape.csv',header=F)
data_cntm_aparc_length=read.csv('connectome_aparc_length_reshape.csv',header=F)
data_cntm_aparc2009_count=read.csv('connectome_aparc2009_count_reshape.csv',header=F)
data_cntm_aparc2009_length=read.csv('connectome_aparc2009_length_reshape.csv',header=F)

data_cntm_subject=read.csv('connectome_subjectlist.csv')
tmp <- data.frame(data_cntm_subject,data_cntm_aparc_count,data_cntm_aparc_length,data_cntm_aparc2009_count,data_cntm_aparc2009_length)
data_2_conn<-merge(data_demo,tmp,by="subject")
data_2_conn<-data_2_conn[]
data_2_conn<-data_2_conn[, colSums(data_2_conn != 0, na.rm = F) > 0] # note that columns with Zeros only will be discarded

t<-data_2_conn[, 12:33709]
y<-as.numeric(data_2_conn[, 2])

correlations <- vapply(
  t,
  function(x)
  {
    cor(y, x)
  },
  numeric(1)
)

correlations[which.max(abs(correlations))]
data_2_conn_rank=rank(correlations)

correlations[rank(correlations)<101]

data_2_conn=data.frame(data_2_conn[,1:11],t)
write.csv(data_2_conn,file='data_2_conn.csv',row.names=F,na="NaN")


data_2_conn_select_100=data.frame(data_2_conn[,1:11],t[,rank(correlations)<101])
write.csv(data_2_conn_select_100,file='data_2_conn_select_100.csv',row.names=F,na="NaN")

data_2_conn_select_500=data.frame(data_2_conn[,1:11],t[,rank(correlations)<501])
write.csv(data_2_conn_select_500,file='data_2_conn_select_500.csv',row.names=F,na="NaN")

data_2_conn_select_1000=data.frame(data_2_conn[,1:11],t[,rank(correlations)<1001])
write.csv(data_2_conn_select_1000,file='data_2_conn_select_1000.csv',row.names=F,na="NaN")

data_2_conn_select_5000=data.frame(data_2_conn[,1:11],t[,rank(correlations)<5001])
write.csv(data_2_conn_select_5000,file='data_2_conn_select_5000.csv',row.names=F,na="NaN")

data_2_conn_select_10000=data.frame(data_2_conn[,1:11],t[,rank(correlations)<100001])
write.csv(data_2_conn_select_10000,file='data_2_conn_select_10000.csv',row.names=F,na="NaN")

data_2_conn_select_20000=data.frame(data_2_conn[,1:11],t[,rank(correlations)<20001])
write.csv(data_2_conn_select_20000,file='data_2_conn_select_20000.csv',row.names=F,na="NaN")

data_2_conn_select_30000=data.frame(data_2_conn[,1:11],t[,rank(correlations)<30001])
write.csv(data_2_conn_select_30000,file='data_2_conn_select_30000.csv',row.names=F,na="NaN")

#data_2_conn_select_40000=data.frame(data_2_conn[,1:11],t[,rank(correlations)<40001])
#write.csv(data_2_conn_select_40000,file='data_2_conn_select_40000.csv',row.names=F,na="NaN")

#data_2_conn_select_50000=data.frame(data_2_conn[,1:11],t[,rank(correlations)<50001])
#write.csv(data_2_conn_select_50000,file='data_2_conn_select_50000.csv',row.names=F,na="NaN")

##############################




## data all
t<-c(data_2_conn[,-2:-11])
data_3_all<-merge(data_1_mor,t,by="subject")
write.table(data_3_all,file="data_3_all.csv",sep = ",",row.names = F)
#nums <- sapply(data_3_all, is.numeric)

#data_3_all_num<-data_3_all[ , nums]
#data_3_all$dx.1ad.2mci.3smi.y=[]



#data_3_all_num_mat<-data.matrix(data_3_all_num)
#data_3_all_num_mat<-data_3_all_num_mat[,2:68859]

#write.table(data_3_all_num_mat,file="data_3_all_num_mat.csv",sep = ",",col.names = NA)



