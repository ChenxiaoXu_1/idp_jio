load('data')

data3all=data3all(:,[1:949, 953:end]);
Y_name=Y_name([1:949, 953:end]);

X=data3all(:,3); 
Y=data3all(:,6:end);

[RHO,PVAL]=corr(X,Y,'rows','pairwise');
ind=abs(RHO)>0.3;
Y_rho_03=Y(:,ind)';
